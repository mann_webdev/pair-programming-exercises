// Information banner
var bannerStatus = window.sessionStorage.getItem('showBanner');

if(bannerStatus == 'hidden') {
  $('.mm-news-banner').hide();
};


function closeNewsBanner(){
  var $mm_info_banner_height = $('.mm-news-banner').outerHeight();
  var $mm_topbar_height = $('.mm-topbar').outerHeight();
  console.log($mm_topbar_height + ' ' + $mm_info_banner_height);

  window.sessionStorage.setItem('showBanner','hidden');
  $('.mm-news-banner').hide();
  if ($(window).width() < 769) {
    if ($('.mm-news-banner').is(":visible")) {
      $('.mm-main-menu-wrapper').css('top', $mm_info_banner_height + $mm_topbar_height);
    } else {
      $('.mm-main-menu-wrapper').css('top', $mm_topbar_height);
    }
  };
}

function mobile_menu_position() {
  var $mm_info_banner_height = $('.mm-news-banner').outerHeight();
  var $mm_topbar_height = $('.mm-topbar').outerHeight();
  console.log($mm_topbar_height + ' ' + $mm_info_banner_height + ' page loaded');
  if ($(window).width() < 769) {
    if ($('.mm-news-banner').is(":visible")) {
      $('.mm-main-menu-wrapper').css('top', $mm_info_banner_height + $mm_topbar_height);
    } else {
      $('.mm-main-menu-wrapper').css('top', $mm_topbar_height);
    }
  };
}

$(window).on('load', mobile_menu_position);


// Loading spinner
$(document).ready(function(){
  $('.mm-page-spinner-wrapper').hide();
  $('.mm-titlebar.mm-lo-home-titlebar').fadeIn();
})

function mm_titlebar_layout() {
  var $mm_masthead = $('#masthead').height();
  $('.mm-titlebar').css('marginTop', $mm_masthead-36);
}


$(window).on('resize', mm_titlebar_layout());

//Mobile menu
$('.menu-item').on('click', function(){
  $('.sub-menu:visible').toggle();
  $(this).children('.sub-menu').toggle();
  $(this).on('click', function(){
    $(this).children('.sub-menu').toggle();
  });
});


$('#mm-close-main-menu').on('click', function(){
  $('.sub-menu:visible').toggle();
});


//Scroll to anchor - about
$('.our-story-link').on('click', function(){
  location.href='/about/#our-story';
  $("html, body").animate({ scrollTop: $('#our-story').offset().top - ($('header#masthead').height() - 80) }, 500);
});
//Scroll to anchor - about
$('.lo-story-link').on('click', function(){
  location.href='/#my-story';
  $("html, body").animate({ scrollTop: $('#my-story').offset().top - ($('header#masthead').height() - 80) }, 500);
});

//Display Posts Plugin
$('.display-posts-listing.grid').addClass('row');

//Main Menu
$('#mm-open-main-menu').on('click', function() {
  $('.mm-main-menu-wrapper').css({
    'display': 'block',
    'opacity': 1
  });
  $('body, html').css('overflowY', 'hidden');
  $('.mm-main-menu li').each(function() {
    $(this).addClass('animated fadeInUp');
  });
})

$('#mm-close-main-menu, #mm-close-main-menu').on('click', function() {
  $('.mm-main-menu-wrapper').css({
    'display': 'none',
    'opacity': 0
  });
  $('body, html').css('overflowY', 'visible');
  $('.mm-main-menu li').each(function() {
    $(this).removeClass('animated fadeInUp');
  });
})

//Homepage Slider
$('.carousel').carousel({
  interval: 5000
});

//LO Form Modal
function openFormModal() {
  $(".los-page-overlay").fadeIn();
  $(".modal-form-container").css('top', '0');
  $("body").css("position", "static");
};

function closeFormModal() {
  $("body").css("position", "unset");
  $(".los-page-overlay").fadeOut("fast");
  $(".modal-form-container").css('top', '-50px');
};

//LO Selector Modal
function openLoSelector() {
  $(".los-page-overlay").fadeIn();
  $(".los-form-container").css('top', '0');
  $("body").css("body", "fixed");
};

function closeLos() {
  $("body").css("body", "static");
  $(".los-page-overlay").fadeOut("fast");
  $(".los-form-container").css('top', '-50px');
  $(".step-yolo").fadeOut("fast", function() {
    $("#lo-select").val("Please Select");
    $(".step-one").fadeIn();
  });
  $(".step-one").fadeIn();
};

function goBack() {
  $(".step-yolo").fadeOut("fast", function() {
    $("#lo-select").val("Please Select");
    $(".step-one").fadeIn();
  })
};

function yolo() {
  $(".step-one").fadeOut("fast", function() {
    $(".step-yolo").fadeIn();
  });
};

function nolo() {
  $("html").css("overflow-y", "initial");
  $("body").css("body", "static");
  $(".los-page-overlay").fadeOut("fast");
  $(".los-form-container").css('top', '-50px');
  $(".step-yolo").fadeOut();
  $(".step-one").fadeIn();
  location.reload();
};

function loSelect() {
  $("#getlo-btn").css({
    'height': '55px',
    'width': '145px'
  }).on("click", function() {
    window.open($("#lo-select").val());
    $("body").css("body", "static");
    $(".los-page-overlay").fadeOut("fast");
    $(".los-form-container").css('top', '-50px');
    $(".step-yolo").fadeOut();
    $(".step-one").fadeIn();
    $("#lo-select").val("Please Select");
    location.reload();
  })
};

$('#menu-item-98:not(.lo-site-1003), #responsive-menu-item-98:not(.lo-site-1003), .los-trigger, .mm-download-now-btn').on('click', function() {
  openLoSelector();
});

$('.mm-download-now-btn').on('click', function() {
  openFormModal();
})


// Subpage scripts
$('nav#blog-nav ul li').on('click', function() {
  $this = $(this);
  $this.css('borderBottom', '4px solid #39f');
  $('nav#blog-nav ul li').not($this).each(function() {
    $(this).css('borderBottom', 'none');
  });
})


// Blog sorting
$('.listing-item').each(function() {
  $(this).addClass('all');
});

$('#sort-all').on('click', function() {
  $('.all').fadeIn();
});

$('.sort-group').on('click', function() {
  var category = $(this).attr('id');
  $('.all').hide();
  $('.listing-item').each(function() {
    if ($(this).find('a').text().indexOf(category) >= 0) {
      $(this).fadeIn();
    }
  });
})

//Blog Posts
$postImg = $('article.post > .post-thumbnail img').attr('src');
$('article.post > .post-thumbnail img').hide();
$('article.post > .post-thumbnail').css('backgroundImage', 'url(' + $postImg + ')');


// Loading spinner
$(window).on('load', function() {
  $('.mm-spinner-wrapper, .mm-spinner').hide();
  $('.container.mm-blog-categories').fadeIn();
})

$(window).on('scroll', function() {
  if($('.mm-topbar').is(":visible")){
    if ($(window).scrollTop() < 50) {
      if ($(window).width() < 992) {
        $('a.custom-logo-link img').css('width', '180px');
      } else {
        $('a.custom-logo-link img').css('width', '240px');
      };
      // $('#masthead').css('height', '160px');
      // $('ul#primary-menu li a').css('height', '92%');
      // $('nav#site-navigation, ul#primary-menu').css('paddingTop', '7px');
      // $('ul.sub-menu').css('marginTop', '-5px');
    } else {
      $('a.custom-logo-link img').css('width', '200px');
      // $('#masthead').css('height', '142px');
      // $('ul#primary-menu li a').css('height', '89%');
      // $('nav#site-navigation, ul#primary-menu').css('paddingTop', '0');
      // $('ul.sub-menu').css('marginTop', '-10px');
    }
  };
  if($('.mm-topbar').is(":hidden")){
    if ($(window).scrollTop() < 50) {
      if ($(window).width() < 992) {
        $('a.custom-logo-link img').css('width', '180px');
      } else {
        $('a.custom-logo-link img').css('width', '240px');
      };
      $('#masthead').css('height', '100px');
      $('ul#primary-menu li a').css('height', '100%');
    } else {
      $('a.custom-logo-link img').css('width', '200px');
      $('#masthead').css('height', '80px');
      $('ul#primary-menu li a').css('height', '91%');
    }
  };
});

//scroll-to-top link
$(window).scroll(function() {
  if ($(window).scrollTop() + $(window).height() > $(document).height() - 100) {
    $('.scroll-up-link').fadeIn();
  } else {
    $('.scroll-up-link').fadeOut();
  }
});

function scrollUp() {
  $("html, body").animate({
    scrollTop: 0
  }, "linear");
  return false;
}

function mmTogglePanel(){

  $('.mm-panel-group').on('click', function(){
    var mmClosePanel = $(this).find('i.fa.fa-minus');

    if ($(this).find('.mm-panel-body').is(':visible')) {
       $('i.fa.fa-minus').hide();
       $('.mm-panel-body').hide();
       $('i.fa.fa-plus').show();
    } else {
      mmClosePanel.show();
      $('.mm-panel-body').hide();
      $('i.fa.fa-minus').not(mmClosePanel).hide();
      $('i.fa.fa-plus').show();
      $(this).find('i.fa.fa-plus').hide();
      $(this).find('.mm-panel-body').css('display', 'block');
      $('html, body').animate({
        scrollTop: $(this).offset().top -180
      }, 'fast');
    }
  });

};

$(window).on('load', function(){
  mmTogglePanel();
});
