<?php
/**
 * manntwentytwenty Theme Customizer
 *
 * @package manntwentytwenty
 */

/**
 * Add postMessage support for site title and description for the Theme Customizer.
 *
 * @param WP_Customize_Manager $wp_customize Theme Customizer object.
 */
function manntwentytwenty_customize_register( $wp_customize ) {
	$wp_customize->get_setting( 'blogname' )->transport         = 'postMessage';
	$wp_customize->get_setting( 'blogdescription' )->transport  = 'postMessage';
	$wp_customize->get_setting( 'header_textcolor' )->transport = 'postMessage';


	if ( isset( $wp_customize->selective_refresh ) ) {
		$wp_customize->selective_refresh->add_partial( 'blogname', array(
			'selector'        => '.site-title a',
			'render_callback' => 'manntwentytwenty_customize_partial_blogname',
		) );
		$wp_customize->selective_refresh->add_partial( 'blogdescription', array(
			'selector'        => '.site-description',
			'render_callback' => 'manntwentytwenty_customize_partial_blogdescription',
		) );
	}


	$wp_customize->add_panel(
		'mm-site-settings',
		array(
			'priority'       => 10,
			'capability'     => 'edit_theme_options',
			'theme_supports' => '',
			'title'          => __('Site Settings', 'manntwentytwenty'),
			'description'    => __('Site Settings', 'manntwentytwenty'),
		)
	);
	$wp_customize->add_section(
		'manntwentytwenty_website_type',
		array(
			'title' => __( 'Website Type', 'manntwentytwenty' ),
			'priority' => 10,
			'capability' => 'edit_theme_options',
			'panel' => 'mm-site-settings',
		)
	);
	$wp_customize->add_section(
		'manntwentytwenty_branch_lo_info',
		array(
			'title' => __( 'Branch/LO Information', 'manntwentytwenty' ),
			'priority' => 10,
			'capability' => 'edit_theme_options',
			'panel' => 'mm-site-settings',
		)
	);
	$wp_customize->add_section(
		'manntwentytwenty_branch_homepage_slider',
		array(
			'title' => __( 'Branch Homepage Slider', 'manntwentytwenty' ),
			'priority' => 10,
			'capability' => 'edit_theme_options',
			'panel' => 'mm-site-settings',
		)
	);
	$wp_customize->add_section(
		'manntwentytwenty_footer_copyright',
		array(
			'title' => __( 'Footer & Copyright Settings', 'manntwentytwenty' ),
			'priority' => 10,
			'capability' => 'edit_theme_options',
			'panel' => 'mm-site-settings'
		)
	);
	/**
  * Settings
	*/
	$wp_customize->add_setting(
		'mm_alert_color',
		array(
			'default' => '',
			'type' => 'theme_mod',
			'transport'   => 'refresh',
		)
	);
	$wp_customize->add_setting(
		'mm_primary_color',
		array(
			'default' => '',
			'type' => 'theme_mod',
			'transport'   => 'refresh',
		)
	);
	$wp_customize->add_setting(
		'mm_secondary_color',
		array(
			'default' => '',
			'type' => 'theme_mod',
			'transport'   => 'refresh',
		)
	);
	$wp_customize->add_setting(
		'mm_intercom_app_id',
		array(
			'default' => '',
			'type' => 'theme_mod',
			'transport'   => 'refresh',
		)
	);
	$wp_customize->add_setting(
		'mm_custom_field_1',
		array(
			'default' => '',
			'type' => 'theme_mod',
			'transport'   => 'refresh',
		)
	);
	$wp_customize->add_setting(
		'mm_slide_1_callout_1',
		array(
			'default' => '',
			'type' => 'theme_mod',
			'transport'   => 'refresh',
		)
	);
	$wp_customize->add_setting(
		'mm_slide_1_callout_2',
		array(
			'default' => '',
			'type' => 'theme_mod',
			'transport'   => 'refresh',
		)
	);
	$wp_customize->add_setting(
		'mm_slide_1_photo',
		array(
			'default' => '',
			'transport'   => 'refresh',
			'type' => 'theme_mod',
			'capability' => 'edit_theme_options',
			'sanitize_callback' => 'absint'
		)
	);
	$wp_customize->add_setting(
		'mm_slide_1_link',
		array(
			'default' => '',
			'type' => 'theme_mod',
			'transport'   => 'refresh',
		)
	);
	$wp_customize->add_setting(
		'mm_slide_2_callout_1',
		array(
			'default' => '',
			'type' => 'theme_mod',
			'transport'   => 'refresh',
		)
	);
	$wp_customize->add_setting(
		'mm_slide_2_callout_2',
		array(
			'default' => '',
			'type' => 'theme_mod',
			'transport'   => 'refresh',
		)
	);
	$wp_customize->add_setting(
		'mm_slide_2_photo',
		array(
			'default' => '',
			'transport'   => 'refresh',
			'type' => 'theme_mod',
			'capability' => 'edit_theme_options',
			'sanitize_callback' => 'absint'
		)
	);
	$wp_customize->add_setting(
		'mm_slide_2_link',
		array(
			'default' => '',
			'type' => 'theme_mod',
			'transport'   => 'refresh',
		)
	);
	$wp_customize->add_setting(
		'mm_slide_3_callout_1',
		array(
			'default' => '',
			'type' => 'theme_mod',
			'transport'   => 'refresh',
		)
	);
	$wp_customize->add_setting(
		'mm_slide_3_callout_2',
		array(
			'default' => '',
			'type' => 'theme_mod',
			'transport'   => 'refresh',
		)
	);
	$wp_customize->add_setting(
		'mm_slide_3_photo',
		array(
			'default' => '',
			'transport'   => 'refresh',
			'type' => 'theme_mod',
			'capability' => 'edit_theme_options',
			'sanitize_callback' => 'absint'
		)
	);
	$wp_customize->add_setting(
		'mm_slide_3_link',
		array(
			'default' => '',
			'type' => 'theme_mod',
			'transport'   => 'refresh',
		)
	);
	$wp_customize->add_setting(
		'mm_slide_4_callout_1',
		array(
			'default' => '',
			'type' => 'theme_mod',
			'transport'   => 'refresh',
		)
	);
	$wp_customize->add_setting(
		'mm_slide_4_callout_2',
		array(
			'default' => '',
			'type' => 'theme_mod',
			'transport'   => 'refresh',
		)
	);
	$wp_customize->add_setting(
		'mm_slide_4_photo',
		array(
			'default' => '',
			'transport'   => 'refresh',
			'type' => 'theme_mod',
			'capability' => 'edit_theme_options',
			'sanitize_callback' => 'absint'
		)
	);
	$wp_customize->add_setting(
		'mm_slide_4_link',
		array(
			'default' => '',
			'type' => 'theme_mod',
			'transport'   => 'refresh',
		)
	);
	$wp_customize->add_setting(
		'mm_website_type',
		array(
			'default' => '',
			'type' => 'theme_mod',
			'transport'   => 'refresh',
		)
	);
	$wp_customize->add_setting(
		'mm_lo_firstname',
		array(
			'default' => '',
			'type' => 'theme_mod',
			'transport'   => 'refresh',
		)
	);
	$wp_customize->add_setting(
		'mm_lo_lastname',
		array(
			'default' => '',
			'type' => 'theme_mod',
			'transport'   => 'refresh',
		)
	);
	$wp_customize->add_setting(
		'mm_team_name',
		array(
			'default' => '',
			'type' => 'theme_mod',
			'transport'   => 'refresh',
		)
	);
	$wp_customize->add_setting(
		'mm_title_position',
		array(
			'default' => '',
			'type' => 'theme_mod',
			'transport'   => 'refresh',
		)
	);
	$wp_customize->add_setting(
		'mm_lo_branch',
		array(
			'default' => '',
			'type' => 'theme_mod',
			'transport'   => 'refresh',
		)
	);
	$wp_customize->add_setting(
		'mm_branch_name',
		array(
			'default' => '',
			'type' => 'theme_mod',
			'transport'   => 'refresh',
		)
	);
	$wp_customize->add_setting(
		'mm_lo_bio',
		array(
			'default' => '',
			'type' => 'theme_mod',
			'transport'   => 'refresh',
		)
	);
	$wp_customize->add_setting(
		'mm_lo_review_1',
		array(
			'default' => '',
			'type' => 'theme_mod',
			'transport'   => 'refresh',
		)
	);
	$wp_customize->add_setting(
		'mm_lo_review_1_name',
		array(
			'default' => '',
			'type' => 'theme_mod',
			'transport'   => 'refresh',
		)
	);
	$wp_customize->add_setting(
		'mm_lo_review_1_location',
		array(
			'default' => '',
			'type' => 'theme_mod',
			'transport'   => 'refresh',
		)
	);
	$wp_customize->add_setting(
		'mm_lo_review_2',
		array(
			'default' => '',
			'type' => 'theme_mod',
			'transport'   => 'refresh',
		)
	);
	$wp_customize->add_setting(
		'mm_lo_review_3',
		array(
			'default' => '',
			'type' => 'theme_mod',
			'transport'   => 'refresh',
		)
	);
	$wp_customize->add_setting(
		'mm_lo_review_2_name',
		array(
			'default' => '',
			'type' => 'theme_mod',
			'transport'   => 'refresh',
		)
	);
	$wp_customize->add_setting(
		'mm_lo_review_2_location',
		array(
			'default' => '',
			'type' => 'theme_mod',
			'transport'   => 'refresh',
		)
	);
	$wp_customize->add_setting(
		'mm_lo_review_3_name',
		array(
			'default' => '',
			'type' => 'theme_mod',
			'transport'   => 'refresh',
		)
	);
	$wp_customize->add_setting(
		'mm_lo_review_3_location',
		array(
			'default' => '',
			'type' => 'theme_mod',
			'transport'   => 'refresh',
		)
	);
	$wp_customize->add_setting(
		'mm_office_phone',
		array(
			'default' => '',
			'type' => 'theme_mod',
			'transport'   => 'refresh',
		)
	);
	$wp_customize->add_setting(
		'mm_mobile_phone',
		array(
			'default' => '',
			'type' => 'theme_mod',
			'transport'   => 'refresh',
		)
	);
	$wp_customize->add_setting(
		'mm_fax',
		array(
			'default' => '',
			'type' => 'theme_mod',
			'transport'   => 'refresh',
		)
	);
	$wp_customize->add_setting(
		'mm_email',
		array(
			'default' => '',
			'type' => 'theme_mod',
			'transport'   => 'refresh',
		)
	);
	$wp_customize->add_setting(
		'mm_nmls',
		array(
			'default' => '',
			'type' => 'theme_mod',
			'transport'   => 'refresh',
		)
	);
	$wp_customize->add_setting(
		'mm_social_survey_widget_id',
		array(
			'default' => '',
			'type' => 'theme_mod',
			'transport'   => 'refresh',
		)
	);
	$wp_customize->add_setting(
		'mm_street',
		array(
			'default' => '',
			'type' => 'theme_mod',
			'transport'   => 'refresh',
		)
	);
	$wp_customize->add_setting(
		'mm_suite',
		array(
			'default' => '',
			'type' => 'theme_mod',
			'transport'   => 'refresh',
		)
	);
	$wp_customize->add_setting(
		'mm_city',
		array(
			'default' => '',
			'type' => 'theme_mod',
			'transport'   => 'refresh',
		)
	);
	$wp_customize->add_setting(
		'mm_state',
		array(
			'default' => '',
			'type' => 'theme_mod',
			'transport'   => 'refresh',
		)
	);
	$wp_customize->add_setting(
		'mm_zip_code',
		array(
			'default' => '',
			'type' => 'theme_mod',
			'transport'   => 'refresh',
		)
	);
	$wp_customize->add_setting(
		'mm_latitude',
		array(
			'default' => '',
			'type' => 'theme_mod',
			'transport'   => 'refresh',
		)
	);
	$wp_customize->add_setting(
		'mm_longitude',
		array(
			'default' => '',
			'type' => 'theme_mod',
			'transport'   => 'refresh',
		)
	);
	$wp_customize->add_setting(
		'mm_alt_1003',
		array(
			'default' => '',
			'type' => 'theme_mod',
			'transport'   => 'refresh',
		)
	);
	$wp_customize->add_setting(
		'mm_alt_login',
		array(
			'default' => '',
			'type' => 'theme_mod',
			'transport'   => 'refresh',
		)
	);
	$wp_customize->add_setting(
		'mm_facebook_url',
		array(
			'default' => '',
			'type' => 'theme_mod',
			'transport'   => 'refresh',
		)
	);
	$wp_customize->add_setting(
		'mm_instagram_url',
		array(
			'default' => '',
			'type' => 'theme_mod',
			'transport'   => 'refresh',
		)
	);
	$wp_customize->add_setting(
		'mm_linkedin_url',
		array(
			'default' => '',
			'type' => 'theme_mod',
			'transport'   => 'refresh',
		)
	);
	$wp_customize->add_setting(
		'mm_lo_photo',
		array(
			'default' => '',
			'type' => 'theme_mod',
			'transport'   => 'refresh',
		)
	);
	$wp_customize->add_setting(
		'mm_lo_photo_two',
		array(
			'default' => '',
			'type' => 'theme_mod',
			'transport'   => 'refresh',
		)
	);
	$wp_customize->add_setting(
		'mm_branch_manager',
		array(
			'default' => '',
			'type' => 'theme_mod',
			'transport'   => 'refresh',
		)
	);
	$wp_customize->add_setting(
		'mm_dba_name',
		array(
			'default' => '',
			'type' => 'theme_mod',
			'transport'   => 'refresh',
		)
	);
	$wp_customize->add_setting(
		'mm_branch_bg_photo',
		array(
			'default' => '',
			'type' => 'theme_mod',
			'transport'   => 'refresh'
		)
	);
	$wp_customize->add_setting(
		'mm_footer_bg_photo',
		array(
			'default' => '',
			'transport'   => 'refresh',
			'type' => 'theme_mod',
			'capability' => 'edit_theme_options',
			'sanitize_callback' => 'absint'
		)
	);
	$wp_customize->add_setting(
		'mm_footer_licensing',
		array(
			'default' => '',
			'type' => 'theme_mod',
			'transport'   => 'refresh',
		)
	);
	/**
	* Controls
	*/
	/**
	* Website Type Control
	*/
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'mm_alert_color', array(
		'section' => 'colors',
		'label'   => esc_html__( 'Alert Topbar Background Color', 'theme' ),
	  ) ) );
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'mm_primary_color', array(
	'section' => 'colors',
	'label'   => esc_html__( 'Primary Brand Color', 'theme' ),
	) ) );
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'mm_secondary_color', array(
	'section' => 'colors',
	'label'   => esc_html__( 'Secondary Brand Color', 'theme' ),
	) ) );
	$wp_customize->add_control(
		'manntwentytwenty_website_type', array(
				'label' => __( 'Website Type', 'manntwentytwenty' ),
				'section' => 'manntwentytwenty_website_type',
				'settings' => 'mm_website_type',
				'type' => 'radio',
				'capability' => 'edit_theme_options',
				'choices' => array(
						'branch' => __( 'Branch' ),
						'loan-officer' => __( 'Loan Officer'),
						'corporate' => __( 'Corporate'),
				)
			)
	);
	/**
	* LO Controls
	*/
	$wp_customize->add_control(
		'mm_lo_firstname_control', array(
				'label' => __( 'Loan Officer First Name:', 'manntwentytwenty' ),
				'section' => 'manntwentytwenty_branch_lo_info',
				'settings' => 'mm_lo_firstname',
				'type' => 'text',
				'capability' => 'edit_theme_options'
			)
	);
	$wp_customize->add_control(
		'mm_lo_lastname_control', array(
				'label' => __( 'Loan Officer Last Name:', 'manntwentytwenty' ),
				'section' => 'manntwentytwenty_branch_lo_info',
				'settings' => 'mm_lo_lastname',
				'type' => 'text',
				'capability' => 'edit_theme_options'
			)
	);
	$wp_customize->add_control(
		'mm_team_name_control', array(
				'label' => __( 'Team Name:', 'manntwentytwenty' ),
				'section' => 'manntwentytwenty_branch_lo_info',
				'settings' => 'mm_team_name',
				'type' => 'text',
				'capability' => 'edit_theme_options'
			)
	);
	$wp_customize->add_control(
		'mm_lo_title_control', array(
				'label' => __( 'Title/Position:', 'manntwentytwenty' ),
				'section' => 'manntwentytwenty_branch_lo_info',
				'settings' => 'mm_title_position',
				'type' => 'text',
				'capability' => 'edit_theme_options'
			)
	);
	$wp_customize->add_control(
		'mm_nmls_control', array(
				'label' => __( 'NMLS #:', 'manntwentytwenty' ),
				'section' => 'manntwentytwenty_branch_lo_info',
				'settings' => 'mm_nmls',
				'type' => 'text',
				'capability' => 'edit_theme_options'
			)
	);
	$wp_customize->add_control(
		'mm_lo_photo_control', array(
				'label' => __( 'LO Photo Link:', 'manntwentytwenty' ),
				'section' => 'manntwentytwenty_branch_lo_info',
				'settings' => 'mm_lo_photo',
				'type' => 'text',
				'capability' => 'edit_theme_options'
			)
	);
	$wp_customize->add_control(
		'mm_lo_photo_two_control', array(
				'label' => __( 'LO Second Photo Link:', 'manntwentytwenty' ),
				'section' => 'manntwentytwenty_branch_lo_info',
				'settings' => 'mm_lo_photo_two',
				'type' => 'text',
				'capability' => 'edit_theme_options'
			)
	);
	$wp_customize->add_control(
		'mm_lo_bio_control', array(
				'label' => __( 'LO Bio/ Branch About:', 'manntwentytwenty' ),
				'section' => 'manntwentytwenty_branch_lo_info',
				'settings' => 'mm_lo_bio',
				'type' => 'textarea',
				'capability' => 'edit_theme_options'
			)
	);
	$wp_customize->add_control(
		'mm_lo_review_1_control', array(
				'label' => __( 'Testimonial #1:', 'manntwentytwenty' ),
				'section' => 'manntwentytwenty_branch_lo_info',
				'settings' => 'mm_lo_review_1',
				'type' => 'text',
				'capability' => 'edit_theme_options'
			)
	);
	$wp_customize->add_control(
		'mm_lo_review_1_name_control', array(
				'label' => __( 'Review #1 by:', 'manntwentytwenty' ),
				'section' => 'manntwentytwenty_branch_lo_info',
				'settings' => 'mm_lo_review_1_name',
				'type' => 'text',
				'capability' => 'edit_theme_options'
			)
	);
	$wp_customize->add_control(
		'mm_lo_review_1_location_control', array(
				'label' => __( 'Review #1 location:', 'manntwentytwenty' ),
				'section' => 'manntwentytwenty_branch_lo_info',
				'settings' => 'mm_lo_review_1_location',
				'type' => 'text',
				'capability' => 'edit_theme_options'
			)
	);
	$wp_customize->add_control(
		'mm_lo_review_2_control', array(
				'label' => __( 'Testimonial #2:', 'manntwentytwenty' ),
				'section' => 'manntwentytwenty_branch_lo_info',
				'settings' => 'mm_lo_review_2',
				'type' => 'text',
				'capability' => 'edit_theme_options'
			)
	);
	$wp_customize->add_control(
		'mm_lo_review_2_name_control', array(
				'label' => __( 'Review #2 by:', 'manntwentytwenty' ),
				'section' => 'manntwentytwenty_branch_lo_info',
				'settings' => 'mm_lo_review_2_name',
				'type' => 'text',
				'capability' => 'edit_theme_options'
			)
	);
	$wp_customize->add_control(
		'mm_lo_review_2_location_control', array(
				'label' => __( 'Review #2 location:', 'manntwentytwenty' ),
				'section' => 'manntwentytwenty_branch_lo_info',
				'settings' => 'mm_lo_review_2_location',
				'type' => 'text',
				'capability' => 'edit_theme_options'
			)
	);
	$wp_customize->add_control(
		'mm_lo_review_3_control', array(
				'label' => __( 'Testimonial #3:', 'manntwentytwenty' ),
				'section' => 'manntwentytwenty_branch_lo_info',
				'settings' => 'mm_lo_review_3',
				'type' => 'text',
				'capability' => 'edit_theme_options'
			)
	);
	$wp_customize->add_control(
		'mm_lo_review_3_name_control', array(
				'label' => __( 'Review #3 by:', 'manntwentytwenty' ),
				'section' => 'manntwentytwenty_branch_lo_info',
				'settings' => 'mm_lo_review_3_name',
				'type' => 'text',
				'capability' => 'edit_theme_options'
			)
	);
	$wp_customize->add_control(
		'mm_lo_review_3_location_control', array(
				'label' => __( 'Review #3 location:', 'manntwentytwenty' ),
				'section' => 'manntwentytwenty_branch_lo_info',
				'settings' => 'mm_lo_review_3_location',
				'type' => 'text',
				'capability' => 'edit_theme_options'
			)
	);
	$wp_customize->add_control(
		'mm_lo_branch_control', array(
				'label' => __( 'Branch:', 'manntwentytwenty' ),
				'section' => 'manntwentytwenty_branch_lo_info',
				'settings' => 'mm_lo_branch',
				'type' => 'text',
				'capability' => 'edit_theme_options'
			)
	);

	/**
	* Branch Controls
	*/
	$wp_customize->add_control(
		'mm_dba_name_control', array(
				'label' => __( 'DBA Name:', 'manntwentytwenty' ),
				'section' => 'manntwentytwenty_branch_lo_info',
				'settings' => 'mm_dba_name',
				'type' => 'text',
				'capability' => 'edit_theme_options'
			)
	);
	$wp_customize->add_control(
		'mm_branch_name_control', array(
				'label' => __( 'Branch Manager:', 'manntwentytwenty' ),
				'section' => 'manntwentytwenty_branch_lo_info',
				'settings' => 'mm_branch_manager',
				'type' => 'text',
				'capability' => 'edit_theme_options'
			)
	);
	$wp_customize->add_control(
		'mm_latitude_control', array(
				'label' => __( 'Latitude:', 'manntwentytwenty' ),
				'section' => 'manntwentytwenty_branch_lo_info',
				'settings' => 'mm_latitude',
				'type' => 'number',
				'capability' => 'edit_theme_options'
			)
	);
	$wp_customize->add_control(
		'mm_longitude_control', array(
				'label' => __( 'Longitude:', 'manntwentytwenty' ),
				'section' => 'manntwentytwenty_branch_lo_info',
				'settings' => 'mm_longitude',
				'type' => 'number',
				'capability' => 'edit_theme_options'
			)
	);
	$wp_customize->add_control(
		'mm_branch_bg_photo_control', array(
				'label' => __( 'Hero Background Image URL:', 'manntwentytwenty' ),
				'section' => 'manntwentytwenty_branch_lo_info',
				'settings' => 'mm_branch_bg_photo',
				'type' => 'text',
				'capability' => 'edit_theme_options'
			)
	);
	$wp_customize->add_control(
		'mm_intercom_app_id_control', array(
				'label' => __( 'Intercomm App_ID:', 'manntwentytwenty' ),
				'section' => 'manntwentytwenty_branch_lo_info',
				'settings' => 'mm_intercom_app_id',
				'type' => 'text',
				'capability' => 'edit_theme_options'
			)
	);

	/**
	* Shared Controls
	*/
	$wp_customize->add_control(
		'mm_office_phone_control', array(
				'label' => __( 'Office Phone:', 'manntwentytwenty' ),
				'section' => 'manntwentytwenty_branch_lo_info',
				'settings' => 'mm_office_phone',
				'type' => 'tel',
				'input_attrs' => array(
						'class' => 'mm-phone-input'
				 ),
				'capability' => 'edit_theme_options'
			)
	);
	$wp_customize->add_control(
		'mm_mobile_phone_control', array(
				'label' => __( 'Mobile Phone:', 'manntwentytwenty' ),
				'section' => 'manntwentytwenty_branch_lo_info',
				'settings' => 'mm_mobile_phone',
				'type' => 'tel',
				'input_attrs' => array(
						'class' => 'mm-phone-input'
				 ),
				'capability' => 'edit_theme_options'
			)
	);
	$wp_customize->add_control(
		'mm_fax_control', array(
				'label' => __( 'Fax Number:', 'manntwentytwenty' ),
				'section' => 'manntwentytwenty_branch_lo_info',
				'settings' => 'mm_fax',
				'type' => 'tel',
				'input_attrs' => array(
						'class' => 'mm-phone-input'
				 ),
				'capability' => 'edit_theme_options'
			)
	);
	$wp_customize->add_control(
		'mm_email_control', array(
				'label' => __( 'Email Address:', 'manntwentytwenty' ),
				'section' => 'manntwentytwenty_branch_lo_info',
				'settings' => 'mm_email',
				'type' => 'email',
				'input_attrs' => array(
						'class' => 'mm-email-input'
				 ),
				'capability' => 'edit_theme_options'
			)
	);

	$wp_customize->add_control(
		'mm_social_survey_id_control', array(
				'label' => __( 'Social Survey Widget ID:', 'manntwentytwenty' ),
				'section' => 'manntwentytwenty_branch_lo_info',
				'settings' => 'mm_social_survey_widget_id',
				'type' => 'text',
				'capability' => 'edit_theme_options'
			)
	);
	$wp_customize->add_control(
		'mm_street_control', array(
				'label' => __( 'Street Address:', 'manntwentytwenty' ),
				'section' => 'manntwentytwenty_branch_lo_info',
				'settings' => 'mm_street',
				'type' => 'text',
				'capability' => 'edit_theme_options'
			)
	);
	$wp_customize->add_control(
		'mm_suite_control', array(
				'label' => __( 'Suite #:', 'manntwentytwenty' ),
				'section' => 'manntwentytwenty_branch_lo_info',
				'settings' => 'mm_suite',
				'type' => 'text',
				'capability' => 'edit_theme_options'
			)
	);
	$wp_customize->add_control(
		'mm_city_control', array(
				'label' => __( 'City:', 'manntwentytwenty' ),
				'section' => 'manntwentytwenty_branch_lo_info',
				'settings' => 'mm_city',
				'type' => 'text',
				'capability' => 'edit_theme_options'
			)
	);
	$wp_customize->add_control(
		'mm_state_control', array(
				'label' => __( 'State:', 'manntwentytwenty' ),
				'section' => 'manntwentytwenty_branch_lo_info',
				'settings' => 'mm_state',
				'type' => 'text',
				'capability' => 'edit_theme_options'
			)
	);
	$wp_customize->add_control(
		'mm_zip_code_control', array(
				'label' => __( 'Zip Code:', 'manntwentytwenty' ),
				'section' => 'manntwentytwenty_branch_lo_info',
				'settings' => 'mm_zip_code',
				'type' => 'text',
				'capability' => 'edit_theme_options'
			)
	);
	$wp_customize->add_control(
		'mm_alt_1003_control', array(
				'label' => __( '1003 URL:', 'manntwentytwenty' ),
				'section' => 'manntwentytwenty_branch_lo_info',
				'settings' => 'mm_alt_1003',
				'type' => 'text',
				'capability' => 'edit_theme_options'
			)
	);
	$wp_customize->add_control(
		'mm_alt_login_control', array(
				'label' => __( 'Alt Login URL:', 'manntwentytwenty' ),
				'section' => 'manntwentytwenty_branch_lo_info',
				'settings' => 'mm_alt_login',
				'type' => 'text',
				'capability' => 'edit_theme_options'
			)
	);
	$wp_customize->add_control(
		'mm_facebook_control', array(
				'label' => __( 'Facebook URL:', 'manntwentytwenty' ),
				'section' => 'manntwentytwenty_branch_lo_info',
				'settings' => 'mm_facebook_url',
				'type' => 'text',
				'capability' => 'edit_theme_options'
			)
	);
	$wp_customize->add_control(
		'mm_instagram_control', array(
				'label' => __( 'Instagram URL:', 'manntwentytwenty' ),
				'section' => 'manntwentytwenty_branch_lo_info',
				'settings' => 'mm_instagram_url',
				'type' => 'text',
				'capability' => 'edit_theme_options'
			)
	);
	$wp_customize->add_control(
		'mm_linkedin_control', array(
				'label' => __( 'LinkedIn URL:', 'manntwentytwenty' ),
				'section' => 'manntwentytwenty_branch_lo_info',
				'settings' => 'mm_linkedin_url',
				'type' => 'text',
				'capability' => 'edit_theme_options'
			)
	);
	$wp_customize->add_control(new WP_Customize_Media_Control($wp_customize,'mm_footer_bg_photo', array(
				'label' => __( 'Footer Background Photo URL:', 'manntwentytwenty' ),
				'section' => 'manntwentytwenty_footer_copyright',
        'mime_type' => 'image'
			)
	));
	$wp_customize->add_control(
		'mm_footer_licensing_control', array(
				'label' => __( 'Footer Licensing Info:', 'manntwentytwenty' ),
				'section' => 'manntwentytwenty_footer_copyright',
				'settings' => 'mm_footer_licensing',
				'type' => 'textarea',
				'capability' => 'edit_theme_options'
			)
	);

	/**
	* Slider Controls
	*/
	$wp_customize->add_control(
		'mm_custom_field_1_control', array(
				'label' => __( 'Schedule Appointment Link:', 'manntwentytwenty' ),
				'section' => 'manntwentytwenty_branch_lo_info',
				'settings' => 'mm_custom_field_1',
				'type' => 'text',
				'capability' => 'edit_theme_options'
			)
	);
	$wp_customize->add_control(
		'mm_slide_1_callout_1_control', array(
				'label' => __( 'Slide 1 Line #1:', 'manntwentytwenty' ),
				'section' => 'manntwentytwenty_branch_homepage_slider',
				'settings' => 'mm_slide_1_callout_1',
				'type' => 'text',
				'capability' => 'edit_theme_options'
			)
	);
	$wp_customize->add_control(
		'mm_slide_1_callout_2_control', array(
				'label' => __( 'Slide 1 Line #2:', 'manntwentytwenty' ),
				'section' => 'manntwentytwenty_branch_homepage_slider',
				'settings' => 'mm_slide_1_callout_2',
				'type' => 'text',
				'capability' => 'edit_theme_options'
			)
	);
	$wp_customize->add_control(new WP_Customize_Media_Control($wp_customize,'mm_slide_1_photo', array(
				'label' => __( 'Slide 1 Background Photo URL:', 'manntwentytwenty' ),
				'section' => 'manntwentytwenty_branch_homepage_slider',
				'mime_type' => 'image'
			)
	));
	$wp_customize->add_control(
		'mm_slide_1_link_control', array(
				'label' => __( 'Slide 1 Button Link', 'manntwentytwenty' ),
				'section' => 'manntwentytwenty_branch_homepage_slider',
				'settings' => 'mm_slide_1_link',
				'type' => 'text',
				'capability' => 'edit_theme_options'
			)
	);
	$wp_customize->add_control(
		'mm_slide_2_callout_1_control', array(
				'label' => __( 'Slide 2 Line #1:', 'manntwentytwenty' ),
				'section' => 'manntwentytwenty_branch_homepage_slider',
				'settings' => 'mm_slide_2_callout_1',
				'type' => 'text',
				'capability' => 'edit_theme_options'
			)
	);
	$wp_customize->add_control(
		'mm_slide_2_callout_2_control', array(
				'label' => __( 'Slide 2 Line #2:', 'manntwentytwenty' ),
				'section' => 'manntwentytwenty_branch_homepage_slider',
				'settings' => 'mm_slide_2_callout_2',
				'type' => 'text',
				'capability' => 'edit_theme_options'
			)
	);
	$wp_customize->add_control(new WP_Customize_Media_Control($wp_customize,'mm_slide_2_photo', array(
				'label' => __( 'Slide 2 Background Photo URL:', 'manntwentytwenty' ),
				'section' => 'manntwentytwenty_branch_homepage_slider',
				'mime_type' => 'image'
			)
	));
	$wp_customize->add_control(
		'mm_slide_2_link_control', array(
				'label' => __( 'Slide 2 Button Link', 'manntwentytwenty' ),
				'section' => 'manntwentytwenty_branch_homepage_slider',
				'settings' => 'mm_slide_2_link',
				'type' => 'text',
				'capability' => 'edit_theme_options'
			)
	);
	$wp_customize->add_control(
		'mm_slide_3_callout_1_control', array(
				'label' => __( 'Slide 3 Line #1:', 'manntwentytwenty' ),
				'section' => 'manntwentytwenty_branch_homepage_slider',
				'settings' => 'mm_slide_3_callout_1',
				'type' => 'text',
				'capability' => 'edit_theme_options'
			)
	);
	$wp_customize->add_control(
		'mm_slide_3_callout_2_control', array(
				'label' => __( 'Slide 3 Line #2:', 'manntwentytwenty' ),
				'section' => 'manntwentytwenty_branch_homepage_slider',
				'settings' => 'mm_slide_3_callout_2',
				'type' => 'text',
				'capability' => 'edit_theme_options'
			)
	);
	$wp_customize->add_control(new WP_Customize_Media_Control($wp_customize,'mm_slide_3_photo', array(
				'label' => __( 'Slide 3 Background Photo URL:', 'manntwentytwenty' ),
				'section' => 'manntwentytwenty_branch_homepage_slider',
				'mime_type' => 'image'
			)
	));
	$wp_customize->add_control(
		'mm_slide_3_link_control', array(
				'label' => __( 'Slide 3 Button Link', 'manntwentytwenty' ),
				'section' => 'manntwentytwenty_branch_homepage_slider',
				'settings' => 'mm_slide_3_link',
				'type' => 'text',
				'capability' => 'edit_theme_options'
			)
	);
	$wp_customize->add_control(
		'mm_slide_4_callout_1_control', array(
				'label' => __( 'Slide 4 Line #1:', 'manntwentytwenty' ),
				'section' => 'manntwentytwenty_branch_homepage_slider',
				'settings' => 'mm_slide_4_callout_1',
				'type' => 'text',
				'capability' => 'edit_theme_options'
			)
	);
	$wp_customize->add_control(
		'mm_slide_4_callout_2_control', array(
				'label' => __( 'Slide 4 Line #2:', 'manntwentytwenty' ),
				'section' => 'manntwentytwenty_branch_homepage_slider',
				'settings' => 'mm_slide_4_callout_2',
				'type' => 'text',
				'capability' => 'edit_theme_options'
			)
	);
	$wp_customize->add_control(new WP_Customize_Media_Control($wp_customize,'mm_slide_4_photo', array(
				'label' => __( 'Slide 4 Background Photo URL:', 'manntwentytwenty' ),
				'section' => 'manntwentytwenty_branch_homepage_slider',
				'mime_type' => 'image'
			)
	));
	$wp_customize->add_control(
		'mm_slide_4_link_control', array(
				'label' => __( 'Slide 4 Button Link', 'manntwentytwenty' ),
				'section' => 'manntwentytwenty_branch_homepage_slider',
				'settings' => 'mm_slide_4_link',
				'type' => 'text',
				'capability' => 'edit_theme_options'
			)
	);
}
add_action( 'customize_register', 'manntwentytwenty_customize_register' );




/**
 * Render the site title for the selective refresh partial.
 *
 * @return void
 */
function manntwentytwenty_customize_partial_blogname() {
	bloginfo( 'name' );
}

/**
 * Render the site tagline for the selective refresh partial.
 *
 * @return void
 */
function manntwentytwenty_customize_partial_blogdescription() {
	bloginfo( 'description' );
}

/**
 * Binds JS handlers to make Theme Customizer preview reload changes asynchronously.
 */
function manntwentytwenty_customize_preview_js() {
	wp_enqueue_script( 'manntwentytwenty-customizer', get_template_directory_uri() . '/js/customizer.js', array( 'customize-preview' ), '20151215', true );
}
add_action( 'customize_preview_init', 'manntwentytwenty_customize_preview_js' );


function theme_get_customizer_css() {
	ob_start();
	
	$mm_alert_color = get_theme_mod( 'mm_alert_color', '' );
	if ( ! empty( $mm_alert_color ) ) {
		?>
		.mm-news-banner {
			background: <?php echo $mm_alert_color; ?>;
		}
		<?php
	}

	$mm_primary_color = get_theme_mod( 'mm_primary_color', '' );
	if ( ! empty( $mm_primary_color ) ) {
		?>
		.btn, .mm-titlebar {
			background: <?php echo $mm_primary_color; ?>;
		}
		ul#primary-menu>li>a:hover {
    		border-bottom: 4px solid <?php echo $mm_primary_color; ?>;
		}

		.mm-nav-apply-btn a {
   			background: <?php echo $mm_primary_color; ?> !important;
		}

		h1.lo-name {
			color: <?php echo $mm_primary_color; ?>;
		}

		p.lo-name {
			color: <?php echo $mm_primary_color; ?>;
		}

		.scheduler-wrapper {
			background: <?php echo $mm_primary_color; ?>;
		}

		.mm-topbar  i {
			color: <?php echo $mm_primary_color; ?>;
		}

		i.fa.fa-minus, i.fa.fa-plus {
			color: <?php echo $mm_primary_color; ?>;
		}

		.mm-titlebar {
			background: <?php echo $mm_primary_color; ?>;
		}

		a.mm-accolades.mm-col-1.mm-a-emulate {
			color: <?php echo $mm_primary_color; ?>;
		}

		.content-page a {
			color: <?php echo $mm_primary_color; ?> !important;
		}
		.mm-slider-btns-wrapper:hover {
			background: <?php echo $mm_primary_color; ?>;
		}
		.mm-borrower-ratings-wrapper a:hover {
			color: <?php echo $mm_primary_color; ?> !important;
		}
		.research-page .mm-cta-btm .btn.theme-button.learn-more,
		.pre-approval-page .mm-cta-btm .btn.theme-button.learn-more,
		.loan-process-page .mm-cta-btm .btn.theme-button.learn-more {
				color: <?php echo $mm_primary_color; ?> !important;
		}

		.research-page .mm-cta-btm .btn.theme-button.learn-more:hover,
		.pre-approval-page .mm-cta-btm .btn.theme-button.learn-more:hover,
		.loan-process-page .mm-cta-btm .btn.theme-button.learn-more:hover  {
			background: <?php echo $mm_primary_color; ?>;
		}

		.mm-blog-filters ul li:hover {
			border-bottom: 4px solid <?php echo $mm_primary_color; ?>;
		}

		#sort-all {
			border-bottom: 4px solid <?php echo $mm_primary_color; ?>;
		}

		div#frm_form_3_container button.frm_button_submit.frm_final_submit {
			background: <?php echo $mm_primary_color; ?>;
		}

		div#frm_form_5_container button.frm_button_submit.frm_final_submit {
			background: <?php echo $mm_primary_color; ?> !important;
		}

		a.btn.cta-btm-btn.mm-download-now-btn {
			color: <?php echo $mm_primary_color; ?> !important;
		}

		.mm-slider-btns-wrapper div:hover {
			border: 1px solid <?php echo $mm_primary_color; ?>;
		}

		@media screen and (max-width: 992px) {
			button#responsive-menu-button {
				background: <?php echo $mm_primary_color; ?> !important;
			}
			.mm-contact-blurb a.cta-btm-btn {
            	background: <?php echo $mm_primary_color; ?> !important;
        	}
		}

		@media screen and (max-width: 768px) {
			.mm-hamburger-box.mm-main-menu-icon.mm-close-menu-icon span {
				background: <?php echo $mm_primary_color; ?>;
			}
		}
		<?php
	}

	$mm_secondary_color = get_theme_mod( 'mm_secondary_color', '' );
	if ( ! empty( $mm_secondary_color ) ) {
		?>
		
		<?php
	}
	
	$css = ob_get_clean();
	return $css;
}


