<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package manntwentytwenty
 */

?>
<?php if ( get_theme_mod("mm_website_type") == 'branch' ) : ?>
<div class="mm-cta-btm">
	<h3 class="mm-tagline">Get Started in Less Than 10 Minutes</h3><h4>Get pre-approved with our online mortgage application. It’s simple, fast &amp; secure!</h4>
	<div class="button-container">
		<a id="los-trigger" class="btn cta-btm-btn" onclick="openLoSelector()">Apply Now</a>
	</div>
</div>
<?php elseif ( get_theme_mod("mm_website_type") == 'loan-officer' ) : ?>
	<div class="mm-cta-btm">
		<h3 class="mm-tagline">Get Started in Less Than 10 Minutes</h3><h4>Get pre-approved with our online mortgage application. It’s simple, fast &amp; secure!</h4>
		<div class="button-container">
			<a id="los-trigger" class="btn cta-btm-btn" href="<?php echo get_theme_mod( 'mm_alt_1003'); ?>">Apply Now</a>
		</div>
	</div>
<?php endif ?>
	</div><!-- #content -->

	<footer id="colophon" class="site-footer">
		<div class="site-info">

		</div>
	</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>

<footer id="main-footer" style="background:url(<?php echo wp_get_attachment_url(get_theme_mod( 'mm_footer_bg_photo')); ?>)">
	<div class="container">
		<div class="privacy-link-wrapper">
		<a href="/privacy">Privacy Policy</a>
		<a href="http://www.nmlsconsumeraccess.org/EntityDetails.aspx/COMPANY/2550">NMLS Consumer Access</a><a href="tel:<?php echo get_theme_mod( 'mm_office_phone'); ?>"><?php echo get_theme_mod( 'mm_office_phone'); ?></a></div>
	  <div class="footer-disclaimers mobile-view">
	    <div id="disclaimer">
	      <p class="mm-disclaimer-wrapper">
				© <?php echo date("Y") ?> <?php echo get_theme_mod( 'mm_dba_name'); ?> Mann Mortgage, LLC | NMLS 2550<br>
	     <?php echo get_theme_mod( 'mm_footer_licensing', 'Alaska (AK2550), Arizona (0907705), California (4130981), Colorado (2550), District of Columbia (MLB2550), Hawaii (HI-2550), Idaho (MBL-437), Maryland (23418), Minnesota (MN-MO-2550), Montana (2550), Nevada (2342), New Mexico (250489), North Carolina (185883), North Dakota (MB101542), Oregon (ML-832), South Dakota (ML.05197), Texas (2550), Utah (7956014), Virginia (MC-6860), Washington (CL-2550), Wisconsin (2550BA), Wyoming (1004). Licensed by the Department of Business Oversight under the California Residential Mortgage Lending Act. Mann Mortgage, LLC is not endorsed by, nor acting on behalf of or at the direction of, the U.S. Department of Housing and Urban Development, Federal Housing Administration, the Veterans Administration, the U.S. Department of Agriculture or the Federal Government. All programs are subject to credit and income qualification. This is not a guarantee of financing or a firm offer of credit.'); ?><br>
	      <?php echo get_theme_mod( 'mm_street'); ?>, <?php echo get_theme_mod( 'mm_suite'); ?>, <?php echo get_theme_mod( 'mm_city'); ?>, <?php echo get_theme_mod( 'mm_state'); ?> <?php echo get_theme_mod( 'mm_zip_code'); ?><br>
	      </p>
	      <div class="">
	        <div class="ehl-logo">
						<img src="/wp-content/themes/manntwentytwenty/assets/mba-logo-gray.png" title="Mortgage Bankers Association" alt="Mortgage Bankers Association logo" style="width: 105px;">
						<img src="/wp-content/themes/manntwentytwenty/assets/eoh-logo-gray.png" title="Equal Housing Opportunity lender" alt="Equal Housing Opportunity logo" style="width: 55px;">
						<img src="/wp-content/themes/manntwentytwenty/assets/30-year-badge-gray.png" alt="30 year anniversary icon" title="30-Year Mortgage Lender" style="width: 53px;margin-left: 5px;margin-top: -21px;margin-bottom: 0px;">
	        </div>
	      </div>
	      <p class=""><?php echo get_theme_mod( 'mm_dba_name'); ?> is an Equal Housing Lender.</p>
	    </div>
	  </div>
	  <div class="footer-social-icons">
	    <ul>
	      <li><a href="<?php echo get_theme_mod( 'mm_facebook_url'); ?>" target="_blank"><i class="footer-icon fa fa-facebook" aria-hidden="true"></i></a></li>
	      <li><a href="<?php echo get_theme_mod( 'mm_linkedin_url'); ?>" target="_blank"><i class="footer-icon fa fa-linkedin" aria-hidden="true"></i></a></li>
	      <li><a href="mailto:<?php echo get_theme_mod( 'mm_email'); ?>" target="_blank"><i class="footer-icon fa fa-envelope" aria-hidden="true"></i></a></li>
	    </ul>
	  </div>
	</div>
  <div class="scroll-up-link" onclick="scrollUp()"><img src="/wp-content/themes/manntwentytwenty/assets/up-chevron.png" alt="Scroll up page arrow"></div>
</footer>
<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB6vvQBZ_girORLU-x7b8Wcul0659JjEBY&callback=initMap">
</script>
<script>

	var varLat;
	var varLng;

	varLat = <?php echo get_theme_mod( 'mm_latitude'); ?>;
	varLng = <?php echo get_theme_mod( 'mm_longitude'); ?>;
	street = "<?php echo get_theme_mod( 'mm_street'); ?>";
	unit = "<?php echo get_theme_mod( 'mm_suite'); ?>"
	city = "<?php echo get_theme_mod( 'mm_city'); ?>";
	zip = "<?php echo get_theme_mod( 'mm_zip_code'); ?>";
	phone = "<?php echo get_theme_mod( 'mm_office_phone'); ?>"
	email = "<?php echo get_theme_mod( 'mm_email'); ?>"


	function initMap(lat, lng) {
		var stateCenter = new google.maps.LatLng(lat, lng);
		var options = {
			zoom: 10,
			center: new google.maps.LatLng(varLat, varLng),
			mapTypeControlOptions: {
				mapTypeIds: [google.maps.MapTypeId.ROADMAP, google.maps.MapTypeId.HYBRID]
			},
			mapTypeControl: false,
			scaleControl: false,
			zoomControl: true
		};
		var map = new google.maps.Map(document.getElementById('map'), options);

		function branchMarkers(props) {
			var marker = new google.maps.Marker({
				position: props.coords,
				map: map,
				visible: true,
				icon: '/wp-content/themes/manntwentytwenty/assets/homeseed-map-pin.png',
				panControl:false,
				zoomControl:false,
				mapTypeControl:false,
				scaleControl:false,
				streetViewControl:false,
				overviewMapControl:false,
				rotateControl:false
			});
			var infoWindow = new google.maps.InfoWindow({
				content: props.address
			});
			marker.addListener('click', function() {
				infoWindow.open(map, marker);
			});
		}

		branchMarkers({
			name: city + ', WA',
			coords: {
				lat: varLat,
				lng: varLng
			},
			address: '<div class="branch-listing">' +
				'<h3>Homeseed Loans - ' + city + ' Branch</h3>' +
				'<address>' +
				street + ', ' + unit + '<br/>' +  city + ', WA' +
				'</address>' +
				'<a href="tel:' + phone + '">' + phone + '</a><br>' +
				'<a href="mailto:' + email + '" target="_blank">' + email + '</a>' +
				'</div>'
		});

	};

		var openMap = function() {
			var lat = parseFloat(varLat);
			var lng = parseFloat(varLng);

			initMap(lat, lng);
		};

		$(window).on('load', openMap);
</script>
<?php if( is_front_page() ) : ?>
	<script>
	$(window).scroll(function() {
		if ($(window).scrollTop() + $(window).height() > $(".three-col-wrapper").offset().top + 160) {
			$('.three-col.three-col1').each(function() {
					var $this = $(this);
					$this.fadeIn();
					$('.three-col.three-col1 > .three-col-icon-box').addClass('animated flipInX');
				});

			 $('.three-col.three-col2').each(function() {
					var $this = $(this);
					$this.delay(150).fadeIn();
					$('.three-col.three-col2 > .three-col-icon-box').addClass('animated flipInX');
				});

				$('.three-col.three-col3').each(function() {
					var $this = $(this);
					$this.delay(250).fadeIn();
					$('.three-col.three-col3 > .three-col-icon-box').addClass('animated flipInX');
				});
		};
	});
	</script>
<?php endif; ?>
<?php
if ( get_theme_mod("mm_topbar_toggle") == 'show' ) :
?>
	<!-- <style type="text/css">
	.mm-topbar {
			display: block;
	}
	#masthead {
    height: 127px;
	}
	nav#site-navigation, ul#primary-menu {
		height: 75px;
    padding-top: 5px;
	}
	</style> -->
<?php
else :
?>
<!-- <style type="text/css">
.mm-topbar {
		display: none;
}

</style> -->
<?php
endif;
?>

<script>(function(){var w=window;var ic=w.Intercom;if(typeof ic==="function"){ic('reattach_activator');ic('update',w.intercomSettings);}else{var d=document;var i=function(){i.c(arguments);};i.q=[];i.c=function(args){i.q.push(args);};w.Intercom=i;var l=function(){var s=d.createElement('script');s.type='text/javascript';s.async=true;s.src='https://widget.intercom.io/widget/hjjpbdxa';var x=d.getElementsByTagName('script')[0];x.parentNode.insertBefore(s,x);};if(w.attachEvent){w.attachEvent('onload',l);}else{w.addEventListener('load',l,false);}}})();

window.Intercom('boot', {
   app_id: "hjjpbdxa"
});
</script>


</body>
</html>
